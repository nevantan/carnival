import { ApolloServer } from 'apollo-server-express';
import typeDefs from './schema/typeDefs';
import resolvers from './schema/resolvers';

export default (firebase) => {
  const context = () => {
    return {
      db: firebase.firestore()
    };
  };

  return new ApolloServer({
    typeDefs,
    resolvers,
    context
  });
};
