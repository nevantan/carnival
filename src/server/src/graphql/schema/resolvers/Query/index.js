export const hello = () => 'world';

export const games = async (parent, args, ctx, info) => {
  return await ctx.db
    .collection('games')
    .limit(10)
    .get()
    .then((snapshot) => {
      const games = [];
      snapshot.forEach((doc) => {
        games.push({
          id: doc.id,
          ...doc.data()
        });
      });
      return games;
    });
};

export default {
  hello,
  games
};
