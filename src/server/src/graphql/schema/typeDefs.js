import { gql } from 'apollo-server-express';

export default gql`
  type Query {
    hello: String
    games: [Game]!
  }

  type Game {
    id: ID!
    name: String!
    slug: String!
    url: String!
  }
`;
