import dotenv from 'dotenv';
dotenv.config();

import admin from 'firebase-admin';

const account = require(process.env.FIREBASE_SERVICE_KEY);

admin.initializeApp({
  credential: admin.credential.cert(account),
  databaseURL: process.env.FIREBASE_DATABASE_URL
});

export default admin;
