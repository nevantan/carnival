// SSR Imports
import React from 'react';
import ReactDOM from 'react-dom/server';
import { ThemeProvider } from 'react-jss';

import App from '../../client/src/components/App';
import theme from '../../client/src/theme';

// Utilities
import fs from 'fs';
import path from 'path';

// Firebase
import firebase from './firebase';

// OpenID Connect
import Provider from 'oidc-provider';
const oidc = new Provider('http://localhost:3000', {
  features: {
    registration: true
  }
});

// GraphQL
import configureGraphQL from './graphql';
const graphQL = configureGraphQL(firebase);

// Setup SSR
const indexFile = fs.readFileSync(
  path.resolve(__dirname, '..', 'public', 'index.html'),
  'utf8'
);

// Web server
import express from 'express';
const PORT = process.env.PORT || 3000;
const app = express();

(async () => {
  // OpenID Connect
  await oidc.initialize({
    clients: [
      {
        client_id: 'test_implicit_app',
        grant_types: ['implicit'],
        response_types: ['id_token'],
        redirect_uris: ['https://testapp/signin-oidc'],
        token_endpoint_auth_method: 'none'
      }
    ]
  });
  app.use('/oidc', oidc.callback);

  // Setup GraphQL endpoint
  graphQL.applyMiddleware({ app });

  // Serve resources
  app.use(express.static(path.resolve(__dirname, '..', 'public')));

  // SSR
  app.get('*', (req, res) => {
    const app = ReactDOM.renderToString(
      <ThemeProvider theme={theme}>
        <App />
      </ThemeProvider>
    );
    res.send(
      indexFile.replace('<div id="app"></div>', `<div id="app">${app}</div>`)
    );
  });

  app.listen(PORT, () => {
    console.log(`Server listening on port ${PORT}...`);
  });
})();
