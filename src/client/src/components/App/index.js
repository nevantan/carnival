// Libraries
import React from 'react';
import injectSheet from 'react-jss';

// Styles
import styles from './style';

// Pages
import IndexPage from '../../pages/IndexPage';

// Components
import { Route } from 'react-router-dom';

export class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const { classes = {} } = this.props;

    return (
      <div className={classes.app}>
        <Route exact path="/" component={IndexPage} />
      </div>
    );
  }
}

export default injectSheet(styles)(App);
