import React from 'react';
import { shallow } from 'enzyme';

import { App } from './';

describe('the App component', () => {
  it('should render properly by default', () => {
    const wrapper = shallow(<App />);
    expect(wrapper).toMatchSnapshot();
  });
});
