// Libraries
import React from 'react';
import injectSheet from 'react-jss';

// Styles
import styles from './style';

// Components
import { Link } from 'react-router-dom';
import { Query } from 'react-apollo';
import { gql } from 'apollo-boost';

export const FETCH_GAMES = gql`
  {
    games {
      name
      slug
    }
  }
`;

export class IndexPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const { classes = {}, games } = this.props;

    return (
      <div className={classes.games}>
        <h1>Games</h1>
        {games.map(({ name, slug }) => (
          <div className={classes.game}>
            <Link key={slug} to={`/game/${slug}`}>
              {name}
            </Link>
          </div>
        ))}
      </div>
    );
  }
}

export const ApolloIndexPage = ({ classes = {} }) => (
  <Query query={FETCH_GAMES}>
    {({ loading, error, data }) => {
      if (loading) return <div className={classes.loading}>Loading...</div>;
      if (error)
        return <div className={classes.error}>Error: {error.message}</div>;

      return <IndexPage classes={classes} games={data.games} />;
    }}
  </Query>
);

export default injectSheet(styles)(ApolloIndexPage);
