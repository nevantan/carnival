import React from 'react';
import { shallow } from 'enzyme';

import { IndexPage } from './';

describe('the IndexPage component', () => {
  it('should render properly by default', () => {
    const wrapper = shallow(<IndexPage />);
    expect(wrapper).toMatchSnapshot();
  });
});
