// Libraries
import React from 'react';
import ReactDOM from 'react-dom';
import configureClient from './graphql';

const client = configureClient();

import { gql } from 'apollo-boost';

// Components
import { BrowserRouter as Router } from 'react-router-dom';
import { ThemeProvider } from 'react-jss';
import { ApolloProvider } from 'react-apollo';
import App from './components/App';

// Styles
import theme from './/theme';

const jsx = (
  <ApolloProvider client={client}>
    <Router>
      <ThemeProvider theme={theme}>
        <App />
      </ThemeProvider>
    </Router>
  </ApolloProvider>
);

const container = document.getElementById('app');

if (container !== null) {
  ReactDOM.render(jsx, container);
}
